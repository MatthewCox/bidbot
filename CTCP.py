'''
Created on Oct 29, 2011

@author: Tyranic-Moron
'''

import Config

def version(conn, user, args):
    conn.ctcp_reply(user, 'VERSION ' + Config.CTCPVersionResponse)

def ping(conn, user, args):
    conn.ctcp_reply(user, 'PING ' + args)