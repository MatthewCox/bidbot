'''
Created on Oct 29, 2011

@author: Tyranic-Moron
'''

import re
import os
import datetime
from pyswitch import Switch

import Config
import Utils
import Users
import AuctionData

def Start(conn, channel, user, auctionID):
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    # If auction already started
    if AuctionData.Progress != -1:
        progressMsg = 'No bids yet'
        if AuctionData.Progress == 0 and AuctionData.HighestBid is not None:
            progressMsg = ( 'Highest Bidder: %s, with %s' %
                            (AuctionData.HighestBid['Nick'],
                             FormattedHighestBid()) )
        elif AuctionData.Progress == 1:
            progressMsg = ( 'Going once to %s, for %s' %
                            (AuctionData.HighestBid['Nick'],
                             FormattedHighestBid()) )
        elif AuctionData.Progress == 2:
            progressMsg = ( 'Going twice to %s, for %s' %
                            (AuctionData.HighestBid['Nick'],
                             FormattedHighestBid()) )
        conn.privmsg( channel,
                      ( '%sThere is already an auction in progress, %s (%s)' %
                        (Config.MessagePrefix,
                         user['Nick'],
                         progressMsg) ) )
    # No auction running yet
    else:
        Utils.UpdateVerifiedList(conn, Config.Channel)
        
        if not os.path.isfile('./get_auction_name.php'):
            Utils.AdminBroadcast( conn,
                                  ( '%s# Auction name script not found (Auction ID is %s)' %
                                    (Config.MessagePrefix,
                                     auctionID) ) )
        else:
            f = os.popen( './get_auction_name.php ' + auctionID )
            response = f.read()
            f.close()
            if response.startswith('FAILED: '):
                Utils.AdminBroadcast( conn,
                                      ( '%s# %s (Auction ID %s)' %
                                        (Config.MessagePrefix,
                                         response,
                                         AuctionData.AuctionID) ) )
            else:
                AuctionData.AuctionName = response
        
        conn.privmsg( Config.Channel,
                      FormatMessage(Config.AuctionStart, user['Nick']) )
        for message in Config.AuctionRules:
            conn.privmsg( Config.Channel, Config.MessagePrefix + message )
        
        AuctionData.AuctionID = auctionID
        AuctionData.Progress = 0
        AuctionData.AuctionStartTime = datetime.datetime.now()

def ParseBid(conn, channel, user, bid):
    if AuctionData.Progress < 0:
        return
    if user['Nick'] not in Users.Verified:
        return
    
    # Match the bid amount
    regex = r'^(?:(?:\d{0,3}(?:[,]\d{0,3])*[,])+\d{3}|\d*)(?:\.\d{0,2})?'
    matchedBid = re.match(regex, bid)
    if matchedBid is None:
        return
    
    stringBid = matchedBid.group(0).replace(',', '')
    if len(stringBid) > 10:
        return
    
    floatBid = float( stringBid )
    
    # Check the bid is valid
    if not ( AuctionData.HighestBid is None or
             ( floatBid >=
               AuctionData.HighestBid['Bid'] + Config.MinimumBidIncrease ) ):
        conn.privmsg(channel,
                     ( '%sThe minimum bid increment is $%.2f (Current bid is %s by %s)' %
                       ( Config.MessagePrefix,
                         Config.MinimumBidIncrease,
                         FormattedHighestBid(),
                         AuctionData.HighestBid['Nick'] ) ) )
        return
    
    # Determine Madness
    appended = IsMadness(floatBid)
    
    # Determine Spacebid
    if AuctionData.HighestBid is not None:
        if user['Nick'] == AuctionData.HighestBid['Nick']:
            appended += ' - SPACE BID!'
    
    # Update the highest bid, and add it to the bid list        
    AuctionData.HighestBid = { 'Nick': user['Nick'], 'Bid': floatBid }
    AuctionData.AllBids.append( AuctionData.HighestBid )
    
    # Say the new highest bid in the channel
    message = FormatMessage(Config.AuctionNewHighestBid,
                            user['Nick']) + appended
    conn.privmsg(Config.Channel, message)
    
    # Reset going once/going twice
    AuctionData.Progress = 0
    
    Log( '%s %s %s' % (user['Nick'],
                       Users.NickIDDict[ user['Nick'] ],
                       FormattedHighestBid()) )

def IsMadness(bid):
    if int(bid / 300) <= AuctionData.MadnessLevel:
        return ''
    
    AuctionData.MadnessLevel = int(bid / 300)
    if AuctionData.MadnessLevel == 1: return ' - THIS IS MADNESS!'
    elif AuctionData.MadnessLevel == 2: return ' - DOUBLE MADNESS!'
    elif AuctionData.MadnessLevel == 3: return ' - TRIPLE MADNESS!'
    elif AuctionData.MadnessLevel == 4: return ' - QUADRUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 5: return ' - QUINTUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 6: return ' - SEXTUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 7: return ' - SEPTUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 8: return ' - OCTUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 9: return ' - NONUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 10: return ' - DECUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 11: return ' - HENDECUPLE MADNESS!'
    elif AuctionData.MadnessLevel == 12: return ' - DUODECUPLE MADNESS!'
    else: return ' - ' + str(AuctionData.MadnessLevel) + 'x MADNESS!'
    
    
def GoingOnce(conn, channel, user):
    if AuctionData.Progress != 0:
        return
    
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    if AuctionData.HighestBid is None:
        conn.privmsg( channel, Config.MessagePrefix + Config.AuctionNoBids )
        return
    
    AuctionData.Progress = 1
    conn.privmsg( Config.Channel,
                  FormatMessage(Config.AuctionGoingOnce, user['Nick']) )
    
    Log('# Going Once #')
    
def GoingTwice(conn, channel, user):
    if AuctionData.Progress != 1:
        return
    
    if AuctionData.HighestBid is None: # Should never happen, but just in case
        return
    
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    AuctionData.Progress = 2
    conn.privmsg( Config.Channel,
                  FormatMessage(Config.AuctionGoingTwice, user['Nick']) )
    
    Log('# Going Twice #')
    
def Sold(conn, channel, user):
    if AuctionData.Progress != 2:
        return
    
    if AuctionData.HighestBid is None: # Should never happen, but just in case
        return
    
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    conn.privmsg( Config.Channel,
                  FormatMessage(Config.AuctionSold, user['Nick']) )
    
    response = ''
    if not os.path.isfile('./save_auction_winner.php'):
        response = './save_auction_winner.php not found'
        Utils.AdminBroadcast( conn,
                              ( '%s# Save auction script not found (Auction ID was %s)' %
                                (Config.MessagePrefix,
                                 AuctionData.AuctionID) ) )
    else:
        f = os.popen( './save_auction_winner.php %s %s %s' %
                      ( AuctionData.AuctionID,
                        Users.NickIDDict[ AuctionData.HighestBid['Nick'] ],
                        AuctionData.HighestBid['Bid'] ) )
        response = f.read()
        f.close()
        Utils.AdminBroadcast( conn,
                              '%s# %s Donor: #%s %s - Prize: #%s %s' %
                              (Config.MessagePrefix,
                               response,
                               Users.NickIDDict[AuctionData.HighestBid['Nick']],
                               AuctionData.HighestBid['Nick'],
                               AuctionData.AuctionID,
                               AuctionData.AuctionName) )
    
    Log( '# Sold (%s) #' % response )
    
    Reset()
    
def GetHighestBid(conn, channel, user):
    if AuctionData.Progress == -1:
        conn.privmsg( user['Nick'],
                      Config.MessagePrefix + 'No auction is running right now' )
        return
    
    if AuctionData.HighestBid is None:
        conn.privmsg( channel,
                      Config.MessagePrefix + Config.AuctionNoBids )
        return
    
    conn.privmsg( channel,
                  FormatMessage( Config.AuctionHighBid, user['Nick'] ) )

def CancelBid(conn, channel, user):
    if AuctionData.Progress == -1:
        return
    if AuctionData.HighestBid is None:
        return
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    Log('# Bid Cancelled, Rolled Back To... #')
    
    if len(AuctionData.AllBids) > 1:
        AuctionData.AllBids.pop()
        AuctionData.HighestBid = AuctionData.AllBids[-1]
        AuctionData.MadnessLevel = int(AuctionData.HighestBid['Bid'] / 300)
        
        Log('%s %s %s' % (AuctionData.HighestBid['Nick'],
                          Users.NickIDDict[ AuctionData.HighestBid['Nick'] ],
                          FormattedHighestBid() ) )
    else:
        AuctionData.HighestBid = None
        AuctionData.MadnessLevel = 0
        
        Log('No Bids')
    
    AuctionData.Progress = 0
    
    conn.privmsg( Config.Channel,
                  FormatMessage( Config.AuctionCancelBid, user['Nick'] ) )

def CancelAuction(conn, channel, user):
    if AuctionData.Progress == -1:
        return
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    Log('# Auction Cancelled #')
    conn.privmsg( Config.Channel,
          FormatMessage( Config.AuctionCancel, user['Nick'] ) )
    Reset()
    
def FormatMessage(message, nick):
    team = ''
    if Config.EnableTeams:
        team = ' #Team' + nick
        
    bidder = 'Nobody'
    bid = 'No bids'
    if AuctionData.HighestBid is not None:
        bidder = AuctionData.HighestBid['Nick']
        bid = FormattedHighestBid()
    
    formatted = Config.MessagePrefix
    formatted += message.substitute(highestBidder=bidder,
                                    highestBid=bid,
                                    auctionName=AuctionData.AuctionName,
                                    user=nick,
                                    teamUser=team)
    return formatted
    
def FormattedHighestBid():
    if AuctionData.HighestBid['Bid'] - int(AuctionData.HighestBid['Bid']) == 0.0:
        return '$%g' % AuctionData.HighestBid['Bid']
    else:
        return '$%.2f' % AuctionData.HighestBid['Bid']
    
def Reset():
    AuctionData.HighestBid = None
    AuctionData.AllBids = []
    AuctionData.Progress = -1
    AuctionData.MadnessLevel = 0
    AuctionData.AuctionID = ''
    AuctionData.AuctionName = ''
    AuctionData.AuctionStartTime = None
    
def Log(text):
    '''0001_2011-11-18_1342.txt'''
    date = AuctionData.AuctionStartTime.strftime('%Y-%m-%d')
    time = AuctionData.AuctionStartTime.strftime('%H%M')
    fileName = './auctionLogs/%s_%s_%s.txt' % (AuctionData.AuctionID.zfill(4),
                                               date,
                                               time)
    Utils.Log(fileName, text)
    
# Handles incoming auction messages, passing them to their respective functions
def Handler(conn, channel, user, args):
    auctionSwitch = Switch()
    
    # Not an auction message
    @auctionSwitch.default
    def gotNothing(value, *args, **kwargs):
        pass
    
    # Pass to Start()
    @auctionSwitch.caseRegEx(r'^start auction (?P<id>[0-9]+)')
    def gotStart(matchObj, *args, **kargs):
        Start(conn, channel, user, matchObj.group('id'))
    
    # Pass to ParseBid()
    @auctionSwitch.caseRegEx(r'^bid([^0-9]+)?(?P<bid>[0-9\,\.]+)')
    def gotBid(matchObj, *args, **kwargs):
        ParseBid(conn, channel, user, matchObj.group('bid'))
    
    # Pass to GoingOnce()
    @auctionSwitch.case('going once')
    def gotGoingOnce(matchObj, *args, **kargs):
        GoingOnce(conn, channel, user)
    
    # Pass to GoingTwice()
    @auctionSwitch.case('going twice')
    def gotGoingTwice(matchObj, *args, **kargs):
        GoingTwice(conn, channel, user)
    
    # Pass to Sold()
    @auctionSwitch.case('sold')
    def gotSold(matchObj, *args, **kargs):
        Sold(conn, channel, user)
        
    # Pass to GetHighestBid()
    @auctionSwitch.case('highbid')
    def gotHigh(matchObj, *args, **kargs):
        GetHighestBid(conn, channel, user)
    
    # Pass to CancelBid()
    @auctionSwitch.case('cancelbid')
    def gotCancelBid(matchObj, *args, **kargs):
        CancelBid(conn, channel, user)
    
    # Pass to CancelAuction()
    @auctionSwitch.case('cancelauction')
    def gotCancel(matchObj, *args, **kargs):
        CancelAuction(conn, channel, user)
    
    auctionSwitch(args.lower())