'''
Created on Oct 29, 2011

@author: Tyranic-Moron
'''

Server = 'irc.desertbus.org'    # The server to connect to
Port = 6667             # The port to connect to
Nick = 'BidBot'         # The bot's nick
Password = 'B1Db0t'     # The bot's password
Email = 'tyranicmoron+bidbot@gmail.com' # The email address the bot will use to register
Username = 'BidBot'     # The bot's username
RealName = 'BidBot'     # The bot's 'real' name
Channel = '#desertbus'  # The channel the bot will connect to
CTCPVersionResponse = 'BidBot v0.6 (created by Tyranic-Moron)' # The string the bot will respond to CTCP VERSION requests with

UpdateInterval = 10 * 60 # Interval in seconds between automatic donor list updates

# Whether or not to output messages to and from the server to the console (requires the bot to be restarted to change)
OutputCommunication = True

# The list of users allowed to use restricted commands
BotAdmins = [ 'Ashton', 'Kroze', 'Fugiman' ]

from Utils import Format, BuildColor
# A prefix to be added to all messages the bot outputs. Useful for formatting
MessagePrefix = Format['Bold'] + Format['Underline'] + BuildColor('Red', None)
"""
You can use the Format dictionary and BuildColor(fore, back)
to bold, underline, and color the messages.

Available formats are:
 - Bold, Underline
eg; Format['Bold']

Available colors are:
 - White, Black, Blue, Green, Red, Brown, Purple, Orange, Yellow,
   Lime, Teal, Cyan, LightBlue, Pink, Grey, LightGrey
eg; BuildColor('Green', 'Blue')
To specify only one color, put unquoted None as whichever color you don't want to specify

These can also be put into the following messages anywhere you wish,
for example to highlight the highest bid in an auction message:
AuctionNewHighestBid = Template('New highest bid: ${highestBidder} at ' + BuildColor('Red', None) + '${highestBid}')
"""

MinimumBidIncrease = 5.0    # The minimum acceptable bid increase in auctions, and the minimum starting bid

# The set of auction rules to output when an auction is started
AuctionRules = ["Make bids by saying 'bid #,###.##'",
                'The minimum increment between bids is $%.2f' % MinimumBidIncrease,
                'Only voiced (registered donor) users can bid',
                'Please do not make any fake bids']

EnableTeams = True

"""
The following are a bunch of templated strings for auction messages.

The available templates are as follows:
${highestBidder} - The current highest bidder
${highestBid} - The current highest bid
${auctionName} - The name of the current auction
${user} - The user who caused the message to appear
${teamUser} - Expands to ' #Team${user}' if EnableTeams is true, otherwise ''
"""
from string import Template
# The text the bot will say when an auction is started
AuctionStart = Template('Auction Started - ${auctionName}')

# The text the bot will say when there is a new highest bid
AuctionNewHighestBid = Template('New highest bid: ${highestBidder} at ${highestBid}')

# The text the bot will say when an auction admin says going once
AuctionGoingOnce = Template('Going Once! (to ${highestBidder} for ${highestBid})${teamUser}')

# The text the bot will say when an auction admin says going twice
AuctionGoingTwice = Template('Going Twice! (to ${highestBidder} for ${highestBid})${teamUser}')

# The text the bot will say when an auction admin says sold
AuctionSold = Template('Sold! (to ${highestBidder} for ${highestBid})${teamUser}')

# The text the bot will say when a user says highbid
AuctionHighBid = Template('The current highest bid is ${highestBid} from ${highestBidder}')

# The text the bot will say when an admin says cancelbid
AuctionCancelBid = Template('Highest bid cancelled, rolled back to ${highestBidder} at ${highestBid}')

# The text the bot will say when an admin says cancelauction
AuctionCancel = Template("Auction for '${auctionName}' cancelled")

# The text the bot will say if you try to use certain commands while there are no bids
AuctionNoBids = 'There are no bids on the current auction'
