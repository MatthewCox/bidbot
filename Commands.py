'''
Created on Oct 28, 2011

@author: Tyranic-Moron
'''

import sys

import Utils
import Users
import Config

# Sends a message to NickServ to check if a user is identified
def do_recheck(conn, channel, user, args):
    if args is None:
        Utils.CheckNicks(conn, [user['Nick']])
        conn.privmsg( user['Nick'],
                      ( '%s# Checked nickserv identify status of: %s' %
                        (Config.MessagePrefix,
                         user['Nick']) ) )
    elif user['Nick'] in Users.VerifiedAdmins:
        Utils.CheckNicks(conn, args)
        Utils.AdminBroadcast( conn,
                              ( '%s# Checked nickserv identify status of: %s' %
                                (Config.MessagePrefix,
                                 ' '.join(args)) ) )

# Updates the Nick to ID map and the verified users list
def do_update(conn, channel, user, args):
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    Utils.UpdateVerifiedList(conn, Config.Channel)
    
def do_forceupdate(conn, channel, user, args):
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    Utils.ForceUpdateVerifiedList(conn, Config.Channel)

def do_reload(conn, channel, user, args):
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    reload(Config)
    Utils.CheckAdmins(conn)
    Utils.AdminBroadcast(conn, Config.MessagePrefix + '# Config File Reloaded')

def do_exit(conn, channel, user, args):
    if user['Nick'] not in Users.VerifiedAdmins:
        return
    
    Utils.AdminBroadcast(conn, '# Bot Exiting')
    sys.exit()