'''
Created on Oct 28, 2011

@author: Tyranic-Moron
'''

import re
import sys
import datetime
import time

import irclib

import Config
import Utils
import CTCP
import Users

import Commands
import Auction

irclib.DEBUG = Config.OutputCommunication

class BidBot(irclib.SimpleIRCClient):
    # Join the channel when the server sends the end of MOTD message
    def on_endofmotd(self, conn, event):
        conn.privmsg('NickServ', 'identify ' + Config.Password)
        conn.join(Config.Channel)
        time.sleep(2.0)
        Utils.UpdateVerifiedList(conn, Config.Channel)
        Users.NextUpdate = ( datetime.datetime.now() +
                             datetime.timedelta(seconds=Config.UpdateInterval) )
    
    # The bot's nickname is currently in use - print a message and exit
    def on_nicknameinuse(self, conn, event):
        print 'A user called ' + Config.Nick + ' is already on the server.'
        sys.exit()
    
    # Process NickServ notices 
    def on_privnotice(self, conn, event):
        # Ignore notices not directed at the bot
        if event.target() != Utils.GetNick(conn):
            return
        # Ignore notices if they are not from NickServ
        if Utils.SplitHostMask( event.source() )['Nick'] != 'NickServ':
            return
        
        # Register if not already registered
        if event.arguments()[0] == "Your nick isn't registered.":
            conn.privmsg( 'NickServ',
                          'register ' + Config.Password + ' ' + Config.Email )
            return
        
        # The notice is a response to a Utils.CheckNicks call
        if event.arguments()[0].startswith('STATUS '):
            Utils.ParseSTATUS(conn, event.arguments()[0])
    
    # Stop Ops from voicing/unvoicing people
    def on_mode(self, conn, event):
        modes = list(event.arguments()[0])
        users = event.arguments()[1:]
        # User(s) being voiced
        if modes[0] == '+':
            for (mode, user) in map(None, modes[1:], users):
                if mode != 'v':
                    continue
                
                if user not in Users.Verified:
                    Utils.CheckNicks(conn, users)
                    Utils.Unvoice(conn, Config.Channel, { 'Nick': user }, None)
        # User(s) being unvoiced
        else:
            for (mode, user) in map(None, modes[1:], users):
                if mode != 'v':
                    continue
                
                if user in Users.Verified:
                    Utils.Voice(conn, Config.Channel, { 'Nick': user }, None)
    
    # Check people when they join the channel
    def on_join(self, conn, event):
        nick = Utils.SplitHostMask( event.source() )['Nick']
        Utils.CheckNicks( conn, [ nick ] )
    
    # Check people when they change their nick
    def on_nick(self, conn, event):
        Utils.CheckNicks( conn, [ event.target() ] )
    
    # Check people when they leave the channel
    # def on_part(self, conn, event):
        # nick = Utils.SplitHostMask( event.source() )['Nick']
        # Utils.CheckNicks( conn, [ nick ] )
    
    # Check people when they quit
    # def on_quit(self, conn, event):
        # nick = Utils.SplitHostMask( event.source() )['Nick']
        # Utils.CheckNicks( conn, [ nick ] )
    
    # Check people when they are kicked
    # def on_kick(self, conn, event):
        # Utils.CheckNicks( conn, [ event.arguments()[0] ] )
    
    # Handle CTCP messages
    def on_ctcp(self, conn, event):
        command = event.arguments()[0].lower()
        args = ''
        if len(event.arguments()) > 1:
            args = event.arguments()[1]
        if hasattr(CTCP, command):
            method = getattr(CTCP, command)
            method(conn, Utils.SplitHostMask( event.source() )['Nick'], args)
    
    # Handle channel messages
    def on_pubmsg(self, conn, event):
        if datetime.datetime.now() > Users.NextUpdate:
            Utils.UpdateVerifiedList(conn, Config.Channel)
            Users.NextUpdate = ( datetime.datetime.now() +
                                 datetime.timedelta(seconds=Config.UpdateInterval) )
        self.HandleCommand(event)
    
    # Handle private messages
    def on_privmsg(self, conn, event):
        self.HandleCommand(event)
    
    # Pass messages to the command or auction handlers
    def HandleCommand(self, event):
        message = event.arguments()[0]
        
        # Check if the message is in command format:
        # <bot nick>(,) <command>( <args>)
        pattern = (Utils.GetNick(self.connection) +
                   ',? (?P<command>[^\s]+) ?(?P<args>.+)?')
        cmdargs = re.match( pattern, message, re.IGNORECASE )
        channel = event.target()
        user = Utils.SplitHostMask( event.source() )
        
        # Pass the message to the auction handler if not in command format
        if cmdargs is None:
            Auction.Handler( self.connection, channel, user, message )
            return
        
        # The message is a command
        command = cmdargs.group('command').lower()
        
        # If the command is 'reloadall'
        if command == 'reloadall' and user['Nick'] in Users.VerifiedAdmins:
            self.ReloadAll()
            Utils.CheckAdmins(self.connection)
            Utils.AdminBroadcast(
                    self.connection,
                    Config.MessagePrefix + '# All Modules Reloaded')
            return
        
        args = cmdargs.group('args')
        if args is not None:
            # Split args into a list by spaces...
            if args.find(' ') != -1:
                args = args.split(' ')
            # ...or just put args into a list if there is only one 
            else:
                args = [args]
        # If the command matches one found in Commands.py, call it
        if hasattr(Commands, 'do_' + command):
            method = getattr(Commands, 'do_' + command)
            method(self.connection, channel, user, args)
    
    # Reload all reloadable code modules
    def ReloadAll(self):
        reload(Config)
        reload(Utils)
        reload(Config)
        reload(Auction)
        reload(Commands)
        reload(CTCP)

# Run the bot if this is the file executed by the user
if __name__ == "__main__":
    bidbot = BidBot()
    bidbot.connect(Config.Server, Config.Port,
                   Config.Nick, Config.Password,
                   Config.Username, Config.RealName)
    bidbot.start()