'''
Created on Oct 28, 2011

@author: Tyranic-Moron
'''

import os
import sets
import simplejson

import Config
import Users

# Send a message to NickServ to check the given nick(s) are identified
def CheckNicks(conn, nicks):
    conn.privmsg('NickServ', 'status ' + ' '.join(nicks))

# Parse the responses to Utils.CheckNicks (NickServ STATUS queries)
def ParseSTATUS(conn, statusMsg):
    _, nick, status = statusMsg.split(' ')
    
    # If not identified
    if status != '3':
        # Remove from verified admins list
        if nick in Users.VerifiedAdmins:
            Users.VerifiedAdmins.remove(nick)
        # Remove from verified users list
        if nick in Users.Verified:
            Users.Verified.discard(nick)
    # If identified
    else:
        # Mark bot admins as verified admins
        if nick in Config.BotAdmins and nick not in Users.VerifiedAdmins:
            Users.VerifiedAdmins.append(nick)
        # Mark registered donors as verified users
        if nick in Users.NickIDDict:
            Users.Verified.add(nick)

    # Voice/Unvoice people depending on whether or not
    # they are in the verified users list
    if nick in Users.Verified:
        Voice(conn, Config.Channel, { 'Nick': nick }, None)
    else:
        Unvoice(conn, Config.Channel, { 'Nick': nick }, None)

# Remove verified admins who are no longer in Config.BotAdmins, and send out
# status queries for any admins who are in Config.BotAdmins but not verified
def CheckAdmins(conn):
    for nick in Users.VerifiedAdmins:
        if nick not in Config.BotAdmins:
            Users.VerifiedAdmins.remove(nick)
    for nick in Config.BotAdmins:
        if nick not in Users.VerifiedAdmins:
            CheckNicks(conn, [nick])

def UpdateVerifiedList(conn, channel):
    if not _UpdateNickIdDict(conn):
        return
    
    _UpdateUnverified(conn, channel)
    
    _UpdateVerified(conn, channel)
    
    AdminBroadcast(conn, Config.MessagePrefix + '# Donors List Updated')

def ForceUpdateVerifiedList(conn, channel):
    if not _UpdateNickIdDict(conn):
        return
    
    _UpdateUnverified(conn, channel)
    
    Users.Verified = sets.Set([])
    _UpdateVerified(conn, channel)
    
    AdminBroadcast(conn, Config.MessagePrefix + '# Donors List Force Updated')

def _UpdateNickIdDict(conn):
    if not os.path.isfile('./fetchlist.php'):
        AdminBroadcast(conn,
                       Config.MessagePrefix + '# Update script not found')
        return False
        
    Users.NickIDDict = simplejson.load(os.popen('./fetchlist.php matchedusers'))
    return True

def _UpdateUnverified(conn, channel):
    # Unvoice users that are in the old list but not in the new
    nickList = []
    for nick in Users.Verified:
        if nick in Users.NickIDDict:
            continue
        
        nickList.append(nick)
        
        if len(nickList) == 6:
            Unvoice(conn, channel, None, nickList)
            nickList = []
    
    if len(nickList) > 0:
        Unvoice(conn, channel, None, nickList)

def _UpdateVerified(conn, channel):
    Identified = simplejson.load(os.popen('./fetchlist.php identifiedusers'))
    NewVerified = []
    Users.VerifiedAdmins = []
    
    # Voice users that are in the new list but not in the old
    nickList = []
    for nick in Identified:
        if nick in Config.BotAdmins:
            Users.VerifiedAdmins.append(nick)
        
        if nick not in Users.NickIDDict:
            continue
        
        NewVerified.append(nick)
        
        if nick in Users.Verified:
            continue
        
        nickList.append(nick)
    
        if len(nickList) == 6:
            Voice(conn, channel, None, nickList)
            nickList = []
    
    if len(nickList) > 0:
        Voice(conn, channel, None, nickList)
    
    Users.Verified = sets.Set(NewVerified)

# Broadcast a message to all verified bot admins
def AdminBroadcast(conn, message):
    for nick in Users.VerifiedAdmins:
        conn.privmsg( nick, message )

# Split a hostmask string into a dictionary with
# 'Nick', 'User', and 'Hostmask' as keys
def SplitHostMask(hostmask):
    user = []
    hostmaskArray = hostmask.split('!')
    user.append(hostmaskArray[0])
    hostmaskArray = hostmaskArray[1].split('@')
    user.append(hostmaskArray[0])
    user.append(hostmaskArray[1])
    user = {'Nick': user[0], 'User': user[1], 'Hostmask': user[2]}
    return user

# Get the bot's current nickname
def GetNick(conn):
    return conn.get_nickname()

# Voices the user, or the specified user(s)
def Voice(conn, channel, user, args):
    if args is None:
        conn.mode(channel, '+v ' + user['Nick'])
    elif len(args) == 1:
        conn.mode(channel, '+v ' + args[0])
    else:
        conn.mode(channel, '+%s %s' % ('v'*len(args), ' '.join(args)) )

# Unvoices the user, or the specified user(s)
def Unvoice(conn, channel, user, args):
    if args is None:
        conn.mode(channel, '-v ' + user['Nick'])
    elif len(args) == 1:
        conn.mode(channel, '-v ' + args[0])
    else:
        conn.mode( channel, '-%s %s' % ('v'*len(args), ' '.join(args)) )

# Logs the given text to the given filename
def Log(fileName, text):
    directory = os.path.dirname(fileName)
    if not os.path.exists(directory):
        os.makedirs(directory)
    f = open(fileName, 'a+')
    f.write(text + '\n')
    f.close()

Format = {'Bold': '\x02', 'Underline': '\x1f'} # Available font formats
# A dictionary of IRC colour codes
Color = {'White': '0', 'Black': '1', 'Blue': '2', 'Green': '3', 'Red': '4',
         'Brown': '5', 'Purple': '6', 'Orange': '7', 'Yellow': '8',
         'Lime': '9', 'Teal': '10', 'Cyan': '11', 'LightBlue': '12',
         'Pink': '13', 'Grey': '14', 'LightGrey': '15'}
def BuildColor(fore=None, back=None):
    if fore is None and back is not None:
        return '\x03,' + Color[back]
    elif fore is not None and back is None:
        return '\x03' + Color[fore]
    
    return '\x03%s,%s' % (Color[fore], Color[back])